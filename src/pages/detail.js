import React, {useEffect, useCallback, useState} from 'react';
import {useParams} from 'react-router-dom';

import {getSubmissionDetail, changeStatusSubmission} from '../actions/detail';

import Layout from '../components/Layout';
import LoadingPopup from '../components/LoadingPopup';

const Detail = () => {
  const [data, setData] = useState({});
  const [status, setStatus] = useState('');
  const [loading, setLoading] = useState(false);
  const {slug} = useParams();

  const request = useCallback(async () => {
    setLoading(true);
    try {
      const response = await getSubmissionDetail(slug);
      setStatus(response.status);
      setData(response);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  }, [slug]);

  useEffect(() => {
    request();
  }, [request]);

  const onChangeSelect = event => {
    setStatus(event.target.value);
  };

  const onClickSave = async () => {
    if (data.status === status) return false;

    setLoading(true);
    try {
      await changeStatusSubmission({
        submissionId: data.id,
        status,
      });
      const response = await getSubmissionDetail(slug);
      setStatus(response.status);
      setData(response);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  return (
    <Layout>
      {loading && <LoadingPopup />}
      <h1 className="text-2xl font-bold text-black-0.87 mb-4">Prize Submission User Detail</h1>
      <div className="max-w-4xl w-full bg-white py-5 px-2.5 rounded grid gap-5 grid-cols-3">
        <div className="text-sm col-span-1">ID</div>
        <div className="text-sm col-span-2">{data.id}</div>
        <div className="text-sm col-span-1">User ID</div>
        <div className="text-sm col-span-2">{data.user_id}</div>
        <div className="text-sm col-span-1">Nama</div>
        <div className="text-sm col-span-2">{data.name}</div>
        <div className="text-sm col-span-1">Email</div>
        <div className="text-sm col-span-2">{data.email}</div>
        <div className="text-sm col-span-1">No Telp</div>
        <div className="text-sm col-span-2">{data.msisdn}</div>
        <div className="text-sm col-span-1">Alamat</div>
        <div className="text-sm col-span-2">{data.delivery_address}</div>
        <div className="text-sm col-span-1">Hadiah</div>
        <div className="text-sm col-span-2">{data?.Prizes?.map((prize, index) => `${prize.name}${index === data?.Prizes?.length - 1 ? '' : ', '}`)}</div>
        <div className="text-sm col-span-1">Status</div>
        <div className="text-sm col-span-2">
          <select value={status} onChange={onChangeSelect}>
            <option value="PENDING">Pending</option>
            <option value="REJECT">Reject</option>
            <option value="DELIVERY">Delivery</option>
          </select>
        </div>
        <div className="text-sm col-span-3 flex justify-end">
          <button onClick={onClickSave} className="bg-scooter text-wildSand px-4 py-1 rounded font-semibold">Simpan</button>
        </div>
      </div>
    </Layout>
  );
};

export default Detail;
