import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';

import {FiEye} from 'react-icons/fi';
import {getSubmission} from '../actions/home';

import Layout from '../components/Layout';

const Home = () => {
  const [submissions, setSubmissions] = useState([]);

  useEffect(() => {
    request();
  }, []);

  const request = async () => {
    try {
      const data = await getSubmission();
      setSubmissions(data);
    } catch (error) {}
  };

  return (
    <Layout>
      <h1 className="text-2xl font-bold text-black-0.87 mb-4">Prize Submission User</h1>
      <div className="overflow-x-auto">
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>User ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>No Telp</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {submissions.map(({id, user_id, name, email, msisdn, status}, index) => (
              <tr key={index}>
                <td>{id}</td>
                <td>{user_id}</td>
                <td>{name}</td>
                <td>{email}</td>
                <td>{msisdn}</td>
                <td>{status}</td>
                <td>
                  <Link to={`/${id}`}><FiEye /></Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Layout>
  );
};

export default Home;
