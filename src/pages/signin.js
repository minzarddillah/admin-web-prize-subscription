import React, {useState} from 'react';
import {isEmpty} from 'lodash';
import { useHistory } from "react-router-dom";

import {login} from '../actions/login';
import {validateEmail} from '../utils/helper';

import Input from '../components/Input';
import LoadingPopup from '../components/LoadingPopup';

const Signin = () => {
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const history = useHistory();

  const onChangeInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    if (name === 'email') {
      setEmail(value);
    } else if (name === 'password') {
      setPassword(value);
    }
  };

  const onClickSignin = async () => {
    let isValidated = true;

    if (isEmpty(email)) {
      setEmailError('Email wajib diisi');
      isValidated = false;
    } else if (!validateEmail(email)) {
      setEmailError('Format email salah');
      isValidated = false;
    }
    if (isEmpty(password)) {
      setPasswordError('Password wajib diisi');
      isValidated = false;
    }

    if (!isValidated) return false;
    else {
      setEmailError('');
      setPasswordError('');
    }

    try {
      setLoadingSubmit(true);
      await login({
        email,
        password,
      });
      setErrorMessage('');
      setLoadingSubmit(false);
      history.push('/');
    } catch (error) {
      setErrorMessage(error.message);
      setLoadingSubmit(false);
    }
  };

  return (
    <div className="items-center justify-center h-screen flex flex-col">
      {loadingSubmit && <LoadingPopup />}
      <div className="max-w-screen-md w-full flex flex-col p-6">
        <img
          alt="ruangguru"
          className="self-center"
          src="https://repayment-cdn.ruangguru.com/assets/d9c3362c2d/Ruangguru.b91b70d74669216eab84416072f3c6b9.svg"
        />
        <h1 className="text-center text-white text-xl font-bold mb-8">
          Admin Prize Subscription
        </h1>

        <Input
          placeholder="Email"
          name="email"
          onChange={onChangeInput}
          value={email}
          error={!!emailError}
          errorMessage={emailError}
        />
        <Input
          placeholder="Kata sandi"
          name="password"
          onChange={onChangeInput}
          value={password}
          error={!!passwordError}
          errorMessage={passwordError}
          type="password"
        />
        <button className="button" onClick={onClickSignin}>Masuk</button>
        <p className="text-xs text-center px-1 font-semibold text-red">
          {errorMessage}
        </p>
      </div>
    </div>
  );
};

export default Signin;
