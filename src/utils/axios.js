import axios from 'axios';

axios.defaults.baseURL = process.env.REACT_APP_API_KEY;
axios.defaults.timeout = 35000; // 30 seconds
axios.interceptors.request.use(
  async (response) => {
    const originalConfig = response;
    const userToken = localStorage.getItem('token');

    if (userToken) {
      originalConfig.headers.Authorization = `Bearer ${userToken}`;
    }
    // originalConfig.headers.token = userToken;
    originalConfig.headers['Access-Control-Allow-Origin'] = '*';
    return originalConfig;
  },
  (error) => Promise.reject(error)
);

export default axios;
