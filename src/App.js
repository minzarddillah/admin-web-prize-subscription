import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

import PrivateRoute from './components/privateRoute';
import PublicRoute from './components/publicRoute';

import Signin from './pages/signin';
import Home from './pages/home';
import Detail from './pages/detail';

function App() {
  return (
    <Router>
      <Switch>
        <PrivateRoute path="/" exact component={Home} />
        <PublicRoute path="/signin" exact component={Signin} />
        <PrivateRoute path="/:slug" exact component={Detail} />
      </Switch>
    </Router>
  );
}

export default App;
