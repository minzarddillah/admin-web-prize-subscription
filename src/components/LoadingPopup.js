import React from 'react';

const LoadingPopup = () => (
  <div className="wrapper-loading-popup">
    <div className="container-loading-popup">
      <div className="activity-indicator" />
      <p>Loading ...</p>
    </div>
  </div>
);

export default LoadingPopup;
