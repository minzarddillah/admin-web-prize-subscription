import React from 'react';

import Navbar from './Navbar';

const Layout = ({children}) => (
  <div>
    <Navbar />
    <div className="lg:ml-80 p-4 flex flex-col">
      {children}
    </div>
  </div>
);

export default Layout;
