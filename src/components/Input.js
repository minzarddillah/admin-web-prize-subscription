import React from 'react';
import cn from 'classnames';

const Input = ({
  value, onChange, error, errorMessage, placeholder, name, type
}) => (
  <>
    <input
      placeholder={placeholder}
      name={name}
      onChange={onChange}
      value={value}
      type={type}
      className={cn('mt-2 mb-1 p-2 rounded border-2', {
        'border-red': error
      })}
    />
    <p className="text-xs text-red px-1">{errorMessage}</p>
  </>
);

export default Input