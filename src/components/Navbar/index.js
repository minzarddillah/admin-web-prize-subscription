import React, {useState} from 'react';
import cn from 'classnames';
import {FaBars} from 'react-icons/fa';
import {HiOutlineLogout} from 'react-icons/hi';
import {AiOutlineClose} from 'react-icons/ai';
import {Link} from 'react-router-dom';
import {IconContext} from 'react-icons';

import {SidebarData} from './sidebardata';

const Navbar = () => {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  const doLogout = () => {
    localStorage.removeItem('token');
  };

  return (
    <IconContext.Provider value={{ color: '#FFF' }}>
      <div className="bg-blackPearl h-20 flex justify-start items-center">
        <Link to="#" className="ml-8 text-3xl bg-none">
          <FaBars onClick={showSidebar} />
        </Link>
      </div>
      <nav 
        className={
          cn('bg-blackPearl w-80 h-screen flex justify-center fixed top-0', {
            'left-0 duration-300': sidebar,
            '-left-full duration-700 lg:left-0': !sidebar
          })
        }
      >
        <ul className="w-full" onClick={showSidebar}>
          <li className="bg-blackPearl w-full h-20 flex justify-start items-center">
            <Link to="#" className="ml-8 text-3xl bg-none">
              <AiOutlineClose className="lg:hidden" />
              <img
                alt="ruangguru"
                className="hidden lg:block"
                src="https://repayment-cdn.ruangguru.com/assets/d9c3362c2d/Ruangguru.b91b70d74669216eab84416072f3c6b9.svg"
              />
            </Link>
          </li>
          {SidebarData.map((item, index) => (
            <li key={index} className="flex justify-start items-center py-2 pl-4 list-none h-14">
              <Link to={item.path} className="no-underline text-wildSand text-lg w-95% h-full flex items-center py-0 px-4 rounded hover:bg-scooter">
                {item.icon}
                <span className="ml-4">{item.title}</span>
              </Link>
            </li>
          ))}
          <li className="flex justify-start items-center py-2 pl-4 list-none h-14">
            <Link to="#" onClick={doLogout} className="no-underline text-wildSand text-lg w-95% h-full flex items-center py-0 px-4 rounded hover:bg-scooter">
              <HiOutlineLogout />
              <span className="ml-4">Logout</span>
            </Link>
          </li>
        </ul>
      </nav>
    </IconContext.Provider>
  );
};

export default Navbar;
