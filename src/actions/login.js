import {get} from 'lodash';

import axios from '../utils/axios';

export const login = body => new Promise(async (resolve, reject) => {
  try {
    const {data} = await axios.post('/admin/login', body);
    localStorage.setItem('token', data.token);
    resolve();
  } catch (error) {
    reject({
      status: get(error, 'response.status', 500),
      message: get(error, 'response.data.message', 'Unexpected error'),
    });
  }
});