import get from 'lodash/get';

import axios from '../utils/axios';

export const getSubmission = () => new Promise(async (resolve, reject) => {
  try {
    const {data} = await axios.get('/submission');
    resolve(data.data);
  } catch (error) {
    reject({
      status: get(error, 'response.status', 500),
      message: get(error, 'response.data.message', 'Unexpected error'),
    });
  }
});