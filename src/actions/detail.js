import get from 'lodash/get';
import axios from '../utils/axios';

export const getSubmissionDetail = id => new Promise(async (resolve, reject) => {
  try {
    const {data} = await axios.get(`/submission/${id}`);
    resolve(data.data);
  } catch (error) {
    reject({
      status: get(error, 'response.status', 500),
      message: get(error, 'response.data.message', 'Unexpected error'),
    });
  }
});

export const changeStatusSubmission = body => new Promise(async (resolve, reject) => {
  try {
    await axios.put('/submission/change-status', body)
    resolve();
  } catch (error) {
    console.log(error);
    reject({
      status: get(error, 'response.status', 500),
      message: get(error, 'response.data.message', 'Unexpected error'),
    });
  }
});