module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        scooter: '#2EB5C0',
        crusta: '#FF823C',
        orange: '#FF6C1A',
        red: '#FF0000',
        blackPearl: '#060B26',
        wildSand: '#F5F5F5',
      },
      width: {
        '95%': '95%',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  important: true,
}
